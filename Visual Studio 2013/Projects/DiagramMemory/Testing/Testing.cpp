#include "stdafx.h"
#include "..\DiagramMemory\Diagram.h"
#include "gtest\gtest.h"

TEST(DiagramConstructors, DefaultConstructor)
{
	dgrm::Diagram testSample;
	ASSERT_ANY_THROW(testSample.getDuration(0));
	ASSERT_ANY_THROW(testSample.getValue(0));
}

TEST(DiagramConstructors, CopyConstructor)
{
	dgrm::Diagram testSample1("111000111000"), testSample2(testSample1);
	ASSERT_EQ(3, testSample2.getEndTime());
	ASSERT_EQ(12, testSample2.getTotalTime());
	for (int i = 0; i < testSample2.getEndTime() + 1; i++) {
		if (i % 2 == 0) {
			ASSERT_EQ('1', testSample2.getValue(i));
			ASSERT_EQ(3, testSample2.getDuration(i));
		}
		else {
			ASSERT_EQ('0', testSample2.getValue(i));
			ASSERT_EQ(3, testSample2.getDuration(i));
		}
	}
}

TEST(DiagramMethods, AssignOperator)
{
	dgrm::Diagram testSample1("111000111000"), testSample2 = testSample1;
	ASSERT_EQ(3, testSample2.getEndTime());
	ASSERT_EQ(12, testSample2.getTotalTime());
	for (int i = 0; i < testSample2.getEndTime() + 1; i++) {
		if (i % 2 == 0) {
			ASSERT_EQ('1', testSample2.getValue(i));
			ASSERT_EQ(3, testSample2.getDuration(i));
		}
		else {
			ASSERT_EQ('0', testSample2.getValue(i));
			ASSERT_EQ(3, testSample2.getDuration(i));
		}
	}
}

TEST(DiagramConstructors, IntMaxConstructor)
{
	dgrm::Diagram testSample2('q');
	ASSERT_EQ('X', testSample2.getValue(0));
	ASSERT_EQ(0, testSample2.getEndTime());
	ASSERT_EQ(testSample2.getMaxTime(), testSample2.getTotalTime());
	for (int i = 1; i < testSample2.getEndTime() + 1; i++){
		ASSERT_EQ(0, testSample2.getDuration(i));
		ASSERT_EQ('X', testSample2.getValue(i));
	}
	dgrm::Diagram testSample3('1', 4);
	ASSERT_EQ('1', testSample3.getValue(0));
	ASSERT_EQ(0, testSample3.getEndTime());
	ASSERT_EQ(4, testSample3.getTotalTime());
	ASSERT_ANY_THROW(dgrm::Diagram testSampleExc('a', 90));
	ASSERT_ANY_THROW(dgrm::Diagram testSampleExc('a', 0));
}

TEST(DiagramConstructor, StringConstructor)
{
	dgrm::Diagram testSample4("111000111qwe111234");
	ASSERT_EQ(5, testSample4.getEndTime());
	ASSERT_EQ(18, testSample4.getTotalTime());
	for (int i = 0; i < 3; i++){

		if (i % 2 == 0) {
			ASSERT_EQ('1', testSample4.getValue(i));
			ASSERT_EQ(3, testSample4.getDuration(i));
		}
		else {
			ASSERT_EQ('0', testSample4.getValue(i));
			ASSERT_EQ(3, testSample4.getDuration(i));
		}
	}
	for (int i = 3; i < 6; i++){
		if (i % 2 == 0) {
			ASSERT_EQ('1', testSample4.getValue(i));
			ASSERT_EQ(3, testSample4.getDuration(i));
		}
		else {
			ASSERT_EQ('X', testSample4.getValue(i));
			ASSERT_EQ(3, testSample4.getDuration(i));
		}
	}
	dgrm::Diagram testSample5("0123456789");
	ASSERT_EQ(2, testSample5.getEndTime());
	ASSERT_EQ(10, testSample5.getTotalTime());
	char j = '0';
	for (int i = 0; i < 2; i++, j++){
		if (i < 2) {
			ASSERT_EQ(j, testSample5.getValue(i));
			ASSERT_EQ(1, testSample5.getDuration(i));
		}
		else {
			ASSERT_EQ('X', testSample5.getValue(i));
			ASSERT_EQ(8, testSample5.getDuration(i));
		}
	}
	char s[81];
	for (int i = 0; i < 81; i++)
		s[i] = 'q';
	ASSERT_ANY_THROW(dgrm::Diagram testSampleExc2(s));
}

TEST(DiagramMethods, LShiftAssign)
{
	dgrm::Diagram testSample8("000001010101");
	testSample8 <<= (5);
	for (int i = 0; i < 7; i++){
		if (i % 2 == 0)
			ASSERT_EQ('1', testSample8.getValue(i));
		else
			ASSERT_EQ('0', testSample8.getValue(i));
		ASSERT_EQ(1, testSample8.getDuration(i));
	}
	ASSERT_ANY_THROW(testSample8 <<= (-1));
	ASSERT_ANY_THROW(testSample8 <<= (testSample8.getTotalTime() + 1));
}

TEST(DiagramMethods, Concat)
{
	dgrm::Diagram testSample6("01"), testSample7("010101");
	testSample6 = testSample6 + (testSample7);

	for (int i = 0; i < testSample6.getEndTime(); i++){
		if (i % 2 == 1) {
			ASSERT_EQ('1', testSample6.getValue(i));
			ASSERT_EQ(1, testSample6.getDuration(i));
		}
		if (i % 2 == 0) {
			ASSERT_EQ('0', testSample6.getValue(i));
			ASSERT_EQ(1, testSample6.getDuration(i));
		}
	}
	dgrm::Diagram testSample8("01000000"), testSample9("010101");
	testSample8 = testSample8 + (testSample9);
	for (int i = 0; i < testSample8.getEndTime(); i++){
		if (i == 2)
			ASSERT_EQ(7, testSample8.getDuration(i));
		else
			ASSERT_EQ(1, testSample8.getDuration(i));
		if (i % 2 == 1)
			ASSERT_EQ('1', testSample8.getValue(i));
		if (i % 2 == 0)
			ASSERT_EQ('0', testSample8.getValue(i));
	}
	dgrm::Diagram testSampleExc("0101010");
	ASSERT_NO_THROW(testSampleExc + (testSampleExc));
	dgrm::Diagram testSampleExc1('1');
	ASSERT_ANY_THROW(testSampleExc + (testSampleExc1));
}

TEST(DiagramMethods, correctReplacement)
{
	dgrm::Diagram testSample1("1010000101"), testSample2("xxxx");
	testSample1 = testSample1(testSample2, 3);
	for (int i = 0; i < testSample1.getEndTime() + 1; i++) {
		if (i == 3){
			ASSERT_EQ('X', testSample1.getValue(i));
			ASSERT_EQ(4, testSample1.getDuration(i));
			continue;
		}
		if (i % 2){
			ASSERT_EQ('0', testSample1.getValue(i));
			ASSERT_EQ(1, testSample1.getDuration(i));
		}
		else{
			ASSERT_EQ('1', testSample1.getValue(i));
			ASSERT_EQ(1, testSample1.getDuration(i));
		}
	}
	dgrm::Diagram testSample3("101XX1"), testSample4("1001");
	testSample3 = testSample3(testSample4, 2);
	for (int i = 0; i < testSample3.getEndTime() + 1; i++) {
		if (i == 2 || i == 4) {
			ASSERT_EQ('1', testSample3.getValue(i));
			ASSERT_EQ(1, testSample3.getDuration(i));
			continue;
		}
		if (i == 3) {
			ASSERT_EQ('0', testSample3.getValue(i));
			ASSERT_EQ(2, testSample3.getDuration(i));
			continue;
		}
		if (i % 2 == 0) {
			ASSERT_EQ('1', testSample3.getValue(i));
			ASSERT_EQ(1, testSample3.getDuration(i));
			continue;
		}
		else {
			ASSERT_EQ('0', testSample3.getValue(i));
			ASSERT_EQ(1, testSample3.getDuration(i));
		}
	}
	dgrm::Diagram testSample5("101xxxx"), testSample6("010101");
	testSample5 = testSample5(testSample6, 3);
	for (int i = 0; i < 9; i++) {
		if (i % 2) {
			ASSERT_EQ('0', testSample5.getValue(i));
			ASSERT_EQ(1, testSample5.getDuration(i));
			continue;
		}
		ASSERT_EQ('1', testSample5.getValue(i));
		ASSERT_EQ(1, testSample5.getDuration(i));
	}
}

TEST(DiagramMethods, RShiftAssign)
{
	dgrm::Diagram testSample8("001");
	testSample8 >>= (3);
	ASSERT_EQ('0', testSample8.getValue(0));
	ASSERT_EQ(5, testSample8.getDuration(0));
	ASSERT_ANY_THROW(testSample8 >>= (-1));
	ASSERT_ANY_THROW(testSample8 >>= (120));
}

TEST(DiagramMethods, RShift)
{
	dgrm::Diagram testSample8("001");
	testSample8 = testSample8 >> (3);
	ASSERT_EQ('0', testSample8.getValue(0));
	ASSERT_EQ(5, testSample8.getDuration(0));
	ASSERT_ANY_THROW(testSample8 >> (-1));
	ASSERT_ANY_THROW(testSample8 >> (120));
}

TEST(DiagramMethods, LShift)
{
	dgrm::Diagram testSample8("000001010101");
	testSample8 = testSample8 << (5);
	for (int i = 0; i < 7; i++){
		if (i % 2 == 0)
			ASSERT_EQ('1', testSample8.getValue(i));
		else
			ASSERT_EQ('0', testSample8.getValue(i));
		ASSERT_EQ(1, testSample8.getDuration(i));
	}
	ASSERT_ANY_THROW(testSample8 << (-1));
	ASSERT_ANY_THROW(testSample8 << (testSample8.getTotalTime() + 1));
}

TEST(DiagramMethods, Copy)
{
	dgrm::Diagram testSample9('z', 2);
	testSample9 = testSample9.copy(10);
	ASSERT_EQ(20, testSample9.getDuration(0));
	dgrm::Diagram testSample10("01");
	testSample10 = testSample10.copy(5);
	for (int i = 0; i < 10; i++){
		if (i % 2 == 0)
			ASSERT_EQ('0', testSample10.getValue(i));
		else
			ASSERT_EQ('1', testSample10.getValue(i));
		ASSERT_EQ(1, testSample10.getDuration(i));
	}
	ASSERT_ANY_THROW(testSample9.copy(-4));
}

int _tmain(int argc, _TCHAR* argv[])
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}

