#pragma once

#include <iostream>
#include <strstream>

namespace dgrm{

	struct Status
	{
		char value;
		int duration;
		Status(char v0 = 'X', int d0 = 0) :value(v0), duration(d0){}
	};

	class Diagram
	{
	private:
		static const int MAX_TIME = 80;
		Status* arrayOfStatus;
		int endTime, totalTime;
		void arrayOptimize();
		void reduceArray();
		void increaseArray(int);
	public:
		void nipOff(int);
		Diagram():arrayOfStatus(nullptr), endTime(0), totalTime(0){};
		Diagram(const Diagram&);
		Diagram(Diagram&&);
		Diagram(const char, int = MAX_TIME);
		Diagram(const char*);
		Diagram& operator=(const Diagram&);
		Diagram& operator=(Diagram&&);
		~Diagram(){ delete[] arrayOfStatus; };
		static int getMaxTime(){ return MAX_TIME; }
		int getTotalTime() const { return totalTime; }
		int getEndTime() const { return endTime; }
		char getValue(int)const;
		int getDuration(int)const;
		bool isEmpty() const { return arrayOfStatus ? false : true; }
		//void print() const;
		const Diagram copy(int) const;
		Diagram& operator<<=(int);
		Diagram& operator>>=(int);
		const Diagram operator<<(int) const;
		const Diagram operator>>(int) const;
		const Diagram operator()(const Diagram&,int) const;
		friend std::ostream& operator<<(std::ostream&, const Diagram&);
		friend std::istream& operator>>(std::istream&, Diagram&);
		friend const Diagram operator+(const Diagram&, const Diagram&);
	};

}
