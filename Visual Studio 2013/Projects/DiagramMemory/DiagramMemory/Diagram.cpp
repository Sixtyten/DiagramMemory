#include "Diagram.h"

namespace dgrm {

	int Diagram::getDuration(int i) const
	{
		if (i < 0 || i > endTime)
			throw std::exception("Invalid num"); 
		return isEmpty() ? throw std::exception("Diagram doesn't exists") : arrayOfStatus[i].duration; 
	}

	char Diagram::getValue(int i) const
	{
		if (i < 0 || i > endTime)
			throw std::exception("Invalid num");
		return isEmpty() ? throw std::exception("Diagram doesn't exists") : arrayOfStatus[i].value;
	}

	Diagram::Diagram(const Diagram& copyValue):
		arrayOfStatus(new Status[copyValue.endTime + 1]),
		endTime(copyValue.endTime),
		totalTime(copyValue.totalTime)
	{
		for (int i = 0; i < endTime + 1; i++) {
			arrayOfStatus[i].value = copyValue.arrayOfStatus[i].value;
			arrayOfStatus[i].duration = copyValue.arrayOfStatus[i].duration;
		}
	}

	Diagram::Diagram(Diagram&& willBeDestroyed) :
		arrayOfStatus(willBeDestroyed.arrayOfStatus), 
		endTime(willBeDestroyed.endTime),
		totalTime(willBeDestroyed.totalTime)
	{
		willBeDestroyed.arrayOfStatus = nullptr;
	}

	Diagram::Diagram(const char v0, int d0) :
		arrayOfStatus(new Status[1]), endTime(0), totalTime(d0)
	{
		if (d0 > MAX_TIME || d0 <= 0)
			throw(std::exception("Invalid time"));
		if (v0 != '1' && v0 != '0')
			arrayOfStatus[0].value = 'X';
		else
			arrayOfStatus[0].value = v0;
		arrayOfStatus[0].duration = d0;
	}

	Diagram& Diagram::operator=(const Diagram& newValue)
	{
		if (this != &newValue) {
			totalTime = newValue.totalTime;
			endTime = newValue.endTime;
			delete[] arrayOfStatus;
			arrayOfStatus = new Status[endTime + 1];
			for (int i = 0; i < endTime + 1; i++) {
				arrayOfStatus[i].value = newValue.arrayOfStatus[i].value;
				arrayOfStatus[i].duration = newValue.arrayOfStatus[i].duration;
			}
		}
		return *this;
	}

	Diagram& Diagram::operator=(Diagram&& willBeDestroyed)
	{
		int buf = endTime;
		endTime = willBeDestroyed.endTime;
		willBeDestroyed.endTime = buf;
		buf = totalTime;
		totalTime = willBeDestroyed.totalTime;
		willBeDestroyed.totalTime = buf;
		Status *ptrBuf = arrayOfStatus;
		arrayOfStatus = willBeDestroyed.arrayOfStatus;
		willBeDestroyed.arrayOfStatus = ptrBuf;
		return *this;
	}

	Diagram::Diagram(const char* str) :
		totalTime(strlen(str))
	{
		int border = 0;
		for (int i = 0; i < strlen(str); i++){
			if (str[i] != str[i + 1] && (str[i] == '0' || str[i] == '1' || str[i + 1] == '0' || str[i + 1] == '1'))
				border++;
		}
		if (!(str[strlen(str) - 1] == '0' || str[strlen(str) - 1] == '1'))
			border++;
		if (strlen(str) - 1 > MAX_TIME)
			throw(std::exception("Invalid string"));
		arrayOfStatus = new Status[border];
		for (int i = 0, j = 0; j < strlen(str); i++, j++) {
			arrayOfStatus[i].duration = 1;
			if (str[j] != '0' && str[j] != '1')
				arrayOfStatus[i].value = 'X';
			else
				arrayOfStatus[i].value = str[j];
			if (str[j] == str[j + 1] || (str[j] != '0' && str[j] != '1' && str[j + 1] != '0' && str[j + 1] != '1'))
				for (; str[j] == str[j + 1] || (str[j] != '0' && str[j] != '1' && str[j + 1] != '0' && str[j + 1] != '1'); j++, arrayOfStatus[i].duration++) {
					if (j == strlen(str) - 1 && (!(str[strlen(str) - 1] == '0' || str[strlen(str) - 1] == '1')))
						break;
				}
			endTime = i;
		}
	}

	//void Diagram::print() const
	//{
	//	for (int i = 0; i < endTime + 1; i++){
	//		std::cout << i << ": duration: " << arrayOfStatus[i].duration << " value : " << arrayOfStatus[i].value << std::endl;
	//	}
	//	std::cout << "endTime: " << endTime << " totalTime: " << totalTime << std::endl;
	//}


	void Diagram::arrayOptimize()
	{
		totalTime = 0;
		for (int i = 0; i < endTime + 1; i++)
			totalTime += arrayOfStatus[i].duration;
	}

	void Diagram::reduceArray()
	{
		Status* newPtr = new Status[endTime];
		for (int i = 0; i < endTime; i++) {
			newPtr[i].duration = arrayOfStatus[i].duration;
			newPtr[i].value = arrayOfStatus[i].value;
		}
		if (endTime)
			endTime--;
		Status* trash = arrayOfStatus;
		arrayOfStatus = newPtr;
		delete[] trash;
	}

	Diagram& Diagram::operator<<=(int time)
	{
		if (time < 0 || time >= totalTime)
			throw(std::exception("Invalid value of time"));
		totalTime -= time;
		for (int i = 0; i < time; i++){
			arrayOfStatus[0].duration--;
			if (arrayOfStatus[0].duration == 0){
				for (int j = 0; j < endTime; j++){
					arrayOfStatus[j].value = arrayOfStatus[j + 1].value;
					arrayOfStatus[j].duration = arrayOfStatus[j + 1].duration;
				}
				arrayOfStatus[endTime].value = 'X';
				arrayOfStatus[endTime].duration = 0;
				reduceArray();
				arrayOptimize();
			}
		}
		return *this;
	}

	const Diagram Diagram::operator<<(int time) const
	{
		Diagram copyValue(*this);
		copyValue <<= time;
		return copyValue;
	}

	void Diagram::increaseArray(int num)
	{
		Status* newPtr = new Status[endTime + 1 + num];
		for (int i = 0; i < endTime + 1; i++) {
			newPtr[i].duration = arrayOfStatus[i].duration;
			newPtr[i].value = arrayOfStatus[i].value;
		}
		endTime += num;
		Status* trash = arrayOfStatus;
		arrayOfStatus = newPtr;
		delete[] trash;
	}

	const Diagram operator+(const Diagram& lValue, const Diagram& rValue)
	{
		if (rValue.totalTime + lValue.totalTime > Diagram::getMaxTime())
			throw(std::exception("Invalid source"));
		if (rValue.arrayOfStatus[0].value == lValue.arrayOfStatus[lValue.endTime].value) {
			Diagram res(lValue);
			res.arrayOfStatus[lValue.endTime].duration += rValue.arrayOfStatus[0].duration;
			res.arrayOptimize();
			if (rValue.endTime == 0)
				return res;
			Diagram copyValue(rValue);
			copyValue <<= copyValue.arrayOfStatus[0].duration;
			return res + copyValue;
		}
		Diagram copyValue(lValue);
		int oldEndTime = copyValue.endTime;
		copyValue.increaseArray(rValue.endTime + 1);
		for (int i = 0; i < rValue.endTime + 1; i++){
			copyValue.arrayOfStatus[i + oldEndTime + 1].value = rValue.arrayOfStatus[i].value;
			copyValue.arrayOfStatus[i + oldEndTime + 1].duration = rValue.arrayOfStatus[i].duration;
		
		}
		copyValue.arrayOptimize();
		return copyValue;
	}

	void Diagram::nipOff(int time)
	{
		totalTime -= time;
		for (int i = 0; i < time; i++){
			arrayOfStatus[endTime].duration--;
			if (arrayOfStatus[endTime].duration == 0){
				arrayOfStatus[endTime].value = 'X';
				reduceArray();
			}
		}
	}

	const Diagram Diagram::operator()(const Diagram& rValue, const int time) const
	{
		if (time >= totalTime || time < 0)
			throw(std::exception("Invalid time"));
		if (time + rValue.totalTime > MAX_TIME)
			throw(std::exception("Not enough time"));
		if (time + rValue.totalTime >= totalTime) {
			if (!time) {
				Diagram res(rValue);
				return res;
			}
			Diagram res(*this);
			res.nipOff(totalTime - time);
			return res + rValue;
		}
		Diagram copyValue(*this);
		copyValue <<= time + rValue.totalTime;
		if (!time)
			return rValue + copyValue;
		Diagram res(*this);
		res.nipOff(totalTime - time);
		res = res + rValue;
		return res + copyValue;

	}

	const Diagram Diagram::copy(int num) const
	{
		if (num <= 0)
			throw(std::exception("Invalid value of num"));
		if (num * totalTime > MAX_TIME)
			throw(std::exception("Not enough space"));
		Diagram copyValueThis(*this);
		Diagram copyValue(*this);
		for (int i = 0; i < num - 1; i++)
			copyValueThis = copyValueThis + copyValue;
		return copyValueThis;
	}

	Diagram& Diagram::operator>>=(int time)
	{
		if (time < 0 || time > MAX_TIME - totalTime)
			throw(std::exception("Invalid value of time"));
		arrayOfStatus[0].duration += time;
		totalTime += time;
		return *this;
	}

	const Diagram Diagram::operator>>(int time) const
	{
		Diagram copyValue(*this);
		copyValue >>= time;
		return copyValue;
	}


	std::ostream& operator<<(std::ostream& outputStream, const Diagram& curSample)
	{
		for (int i = 0, t = 0; i < curSample.endTime + 1; i++){
			for (int k = 0; k < t; k++)
				std::cout << " ";
			for (int j = 0; j < curSample.arrayOfStatus[i].duration; j++, t++)
				std::cout << curSample.arrayOfStatus[i].value;
			std::cout << std::endl;
		}
		return outputStream;
	}

	char* bufferRealloc(char* buf, int newsz)
	{
		char *res = new char[newsz];
		memset(res, 0, newsz);
		strcpy(res, buf);
		return res;
	}

	std::istream& getLine(std::istream& is, char *&buffer)
	{
		std::istream::sentry s(is);
		if (s){
			std::istreambuf_iterator<char> it(is);
			std::istreambuf_iterator<char> end;
			size_t size = 80;
			size_t grow = 64;
			size_t len = 0;
			buffer = new char[size];
			while (it != end && *it != '\n'){
				if (len == (size - 1)){
					buffer = bufferRealloc(buffer, size + grow);
					size += grow;
				}
				buffer[len++] = *it++;
			}
			buffer[len] = '\0';
		}
		return is;
	}

	std::istream& operator>>(std::istream& inputStream, Diagram& currentDiagram)
	{
		char* str = nullptr;
		getLine(inputStream, str);
		Diagram copyValue(str);
		currentDiagram = copyValue;
		return inputStream;
	}
}