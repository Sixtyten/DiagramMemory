#include "stdafx.h"
#include "..\DiagramMemory\Diagram.h" 

int getInt(int &a) {
	std::cin >> a;
	if (std::cin.eof())
		return -2;
	if (!std::cin.good()){
		std::cin.clear();
		std::cin.ignore(std::cin.rdbuf()->in_avail());
		return -1;
	}
	return 1;
}

int dialog(const char *msgs[], int N)
{
	char *errmsg = "";
	int rc;
	int i, n;
	do{
		if (errmsg != "")
			puts(errmsg);
		errmsg = "You are wrong. Repeate, please!";
		for (i = 0; i < N; ++i)
			puts(msgs[i]);
		puts("Make your choice: --> ");
		n = getInt(rc);
		if (n == 0)
			rc = 0;
		if (n == -2)
			exit(0);
	} while (rc < 0 || rc >= N);
	return rc;
}

int inputDiagram(dgrm::Diagram &testSample)
{
	std::cout << "Input diagram: " << std::endl;
	try {
		std::cin >> testSample;
	}
	catch (std::exception& bm){
		std::cout << bm.what() << std::endl;
	}
	return 1;
}

int printDiagram(dgrm::Diagram &testSample)
{
	std::cout << "Diagram: " << std::endl;
	std::cout << testSample;
	return 1;
}

int concatDiagram(dgrm::Diagram &testSample)
{
	if (testSample.isEmpty()){
		std::cout << "You must input diagram firstly" << std::endl;
		return 1;
	}
	dgrm::Diagram rValue;
	std::cout << "Input diagram: " << std::endl;
	try{
		std::cin >> rValue;
		std::cout << testSample + rValue;
	}
	catch (std::exception& bm){
		std::cout << bm.what() << std::endl;
	}
	return 1;
}

int replacementDiagram(dgrm::Diagram &testSample)
{
	if (testSample.isEmpty()){
		std::cout << "You must input diagram firstly" << std::endl;
		return 1;
	}
	dgrm::Diagram rValue;
	std::cout << "Input diagram: " << std::endl;
	try{
		std::cin >> rValue;
	}
	catch (std::exception& bm){
		std::cout << bm.what() << std::endl;
	}
	std::cout << "Enter time: " << std::endl;
	int time = 0, rc = -1, n = 0;
	const char* errmsg = "";
	do{
		if (errmsg != "")
			puts(errmsg);
		errmsg = "You are wrong. Repeate, please!";
		n = getInt(time);
		if (n == 1)
			rc = 0;
		if (n == -2)
			exit(0);
	} while (rc < 0);
	try{
		std::cout << testSample(rValue, time);
	}
	catch (std::exception& bm){
		std::cout << bm.what() << std::endl;
	}
	return 1;
}

int copyDiagram(dgrm::Diagram &testSample)
{
	if (testSample.isEmpty()){
		std::cout << "You must input diagram firstly" << std::endl;
		return 1;
	}
	std::cout << "Enter time: " << std::endl;
	int time = 0, rc = -1, n = 0;
	const char* errmsg = "";
	do{
		if (errmsg != "")
			puts(errmsg);
		errmsg = "You are wrong. Repeate, please!";
		n = getInt(time);
		if (n == 1)
			rc = 0;
		if (n == -2)
			exit(0);
	} while (rc < 0);
	try{
		std::cout << testSample.copy(time);
	}
	catch (std::exception& bm){
		std::cout << bm.what() << std::endl;
	}
	return 1;
}

int lShiftDiagram(dgrm::Diagram &testSample)
{
	if (testSample.isEmpty()){
		std::cout << "You must input diagram firstly" << std::endl;
		return 1;
	}
	std::cout << "Enter time: " << std::endl;
	int time = 0, rc = -1, n = 0;
	const char* errmsg = "";
	do{
		if (errmsg != "")
			puts(errmsg);
		errmsg = "You are wrong. Repeate, please!";
		n = getInt(time);
		if (n == 1)
			rc = 0;
		if (n == -2)
			exit(0);
	} while (rc < 0);
	try{
		std::cout << (testSample << (time));
	}
	catch (std::exception& bm){
		std::cout << bm.what() << std::endl;
	}
	return 1;
}

int rShiftDiagram(dgrm::Diagram &testSample)
{
	if (testSample.isEmpty()){
		std::cout << "You must input diagram firstly" << std::endl;
		return 1;
	}
	std::cout << "Enter time: " << std::endl;
	int time = 0, rc = -1, n = 0;
	const char* errmsg = "";
	do{
		if (errmsg != "")
			puts(errmsg);
		errmsg = "You are wrong. Repeate, please!";
		n = getInt(time);
		if (n == 1)
			rc = 0;
		if (n == -2)
			exit(0);
	} while (rc < 0);
	try{
		std::cout << (testSample >> (time));
	}
	catch (std::exception& bm){
		std::cout << bm.what() << std::endl;
	}
	return 1;
}

int newApplication(dgrm::Diagram& testSample)
{
	dgrm::Diagram firstSample;
	std::cout << "Input diagram: " << std::endl;
	try{
		std::cin >> firstSample;
	}
	catch (std::exception& bm){
		std::cout << bm.what() << std::endl;
	}
	{
		dgrm::Diagram secondSample(firstSample), replaceTest;
		std::cout << "Input replace: " << std::endl;
		try{
			std::cin >> replaceTest;
		}
		catch (std::exception& bm){
			std::cout << bm.what() << std::endl;
		}
		std::cout << replaceTest;

		std::cout << "Enter time: " << std::endl;
		int time = 0, rc = -1, n = 0;
		const char* errmsg = "";
		do{
			if (errmsg != "")
				puts(errmsg);
			errmsg = "You are wrong. Repeate, please!";
			n = getInt(time);
			if (n == 1)
				rc = 0;
			if (n == -2)
				exit(0);
		} while (rc < 0);
		try{
			secondSample = secondSample(replaceTest, time);
			std::cout << secondSample;
		}
		catch (std::exception& bm){
			std::cout << bm.what() << std::endl;
		}
		std::cout << "Enter time: " << std::endl;
		int time1 = 0, rc1 = -1, n1 = 0;
		const char* errmsg1 = "";
		do{
			if (errmsg1 != "")
				puts(errmsg1);
			errmsg1 = "You are wrong. Repeate, please!";
			n1 = getInt(time1);
			if (n1 == 1)
				rc1 = 0;
			if (n1 == -2)
				exit(0);
		} while (rc1 < 0);
		try{
			secondSample <<= time1;
			std::cout << secondSample;
		}
		catch (std::exception& bm){
			std::cout << bm.what() << std::endl;
		}
	}
	std::cout << "First:" << std::endl;
	std::cout << firstSample;
	return 1;
}

int _tmain(int argc, _TCHAR* argv[])
{
	dgrm::Diagram testSample;
	const char *msgs[] = { "0. Quit", "1.Input diagram", "2.Print diagram", "3.Concatenate diagrams", "4.Replace diagrams", "5.Copy diagram", "6.Left shift", "7.Right shift", "8.Test Application" };
	const int NMsgs = sizeof(msgs) / sizeof(msgs[0]);
	int rc;
	int(*fptr[])(dgrm::Diagram&) = { nullptr, inputDiagram, printDiagram, concatDiagram, replacementDiagram, copyDiagram, lShiftDiagram, rShiftDiagram, newApplication };
	while (rc = dialog(msgs, NMsgs))
		if (!fptr[rc](testSample))
			break;
	std::cout << "That's all. Bye!" << std::endl;
	return 0;
}